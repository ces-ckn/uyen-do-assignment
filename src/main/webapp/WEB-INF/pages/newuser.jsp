<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
  <%--<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">--%>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../../favicon.ico">
  <title>Admin page</title>
  <link href="<c:url value='/static/css/bootstrap.min.css' />" rel="stylesheet"/>
  <link href="http://getbootstrap.com/examples/dashboard/dashboard.css" rel="stylesheet">
  <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Craft Beer App</a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="/">Home</a></li>
        <li><a href="/admin">Admin</a></li>
        <li><a href="/dba">Dba</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>
<div class="container" style="background-color: white;">
  <div class="row">
    <div class="col-sm-3 col-md-2 sidebar">
      <ul class="nav nav-list">
        <li class="nav-header">Side Bar</li>
        <li>
          <a href="<c:url value='/admin' />">User</a>
        </li>
        <li>
          <a href="<c:url value='/admin/category' />">Category</a>
        </li>

        <li>
          <a href="<c:url value='/admin/beer' />">Beer</a>
        </li>

        <li>
          <a href="<c:url value="/logout" />">Logout</a>
        </li>
      </ul>
    </div>

    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <div>

        <h1>New User Registration Form</h1>
        <c:url var="addAction" value="/admin/user/add" ></c:url>
        <form:form method="POST" modelAttribute="user" class="form-horizontal" action="${addAction}">

          <div class="row">
            <div class="form-group col-md-12">
              <label class="col-md-3 control-lable" for="firstName">First Name</label>
              <div class="col-md-7">
                <form:hidden path="id" />
                <form:input type="text" path="firstName" id="firstName" class="form-control input-sm"/>
                <div class="has-error">
                  <form:errors path="firstName" class="help-inline"/>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-md-12">
              <label class="col-md-3 control-lable" for="lastName">Last Name</label>
              <div class="col-md-7">
                <form:input type="text" path="lastName" id="lastName" class="form-control input-sm"/>
                <div class="has-error">
                  <form:errors path="lastName" class="help-inline"/>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-md-12">
              <label class="col-md-3 control-lable" for="userName">User Name</label>
              <div class="col-md-7">
                <form:input type="text" path="userName" id="userName" class="form-control input-sm"/>
                <div class="has-error">
                  <form:errors path="userName" class="help-inline"/>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-md-12">
              <label class="col-md-3 control-lable" for="password">Password</label>
              <div class="col-md-7">
                <form:input type="password" path="password" id="password" class="form-control input-sm"/>
                <div class="has-error">
                  <form:errors path="password" class="help-inline"/>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-md-12">
              <label class="col-md-3 control-lable" for="email">Email</label>
              <div class="col-md-7">
                <form:input type="text" path="email" id="email" class="form-control input-sm"/>
                <div class="has-error">
                  <form:errors path="email" class="help-inline"/>
                </div>
              </div>
            </div>
          </div>


          <div class="row">
            <div class="form-group col-md-12">
              <label class="col-md-3 control-lable" for="roles">Roles</label>
              <div class="col-md-7">
                <form:select path="roles" items="${roles}" multiple="true" itemValue="id" itemLabel="type" class="form-control input-sm"/>
                <div class="has-error">
                  <form:errors path="roles" class="help-inline"/>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-actions floatRight">
              <input type="submit" value="Register" class="btn btn-primary btn-sm"> or <a href="<c:url value='/admin' />">Cancel</a>
            </div>
          </div>
        </form:form>
      </div>
    </div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>


