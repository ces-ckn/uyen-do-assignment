<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <%--<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">--%>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>Admin page</title>
    <link href="<c:url value='/static/css/bootstrap.min.css' />" rel="stylesheet"/>
    <link href="http://getbootstrap.com/examples/dashboard/dashboard.css" rel="stylesheet">
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Craft Beer App</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Home</a></li>
                <li><a href="/admin">Admin</a></li>
                <li><a href="/dba">Dba</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<div class="container" style="background-color: white;">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-list">
                <li class="nav-header">Side Bar</li>
                <li>
                    <a href="<c:url value='/admin' />">User</a>
                </li>
                <li>
                    <a href="<c:url value='/admin/category' />">Category</a>
                </li>

                <li>
                    <a href="<c:url value='/admin/beer' />">Beer</a>
                </li>

                <li>
                    <a href="<c:url value="/logout" />">Logout</a>
                </li>
            </ul>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <c:if test="${success != null}">
                <div class="alert alert-success" role="alert">${success}</div>
            </c:if>

            <c:if test="${warning != null}">
                <div class="alert alert-warning" role="alert">${warning}</div>
            </c:if>

            <table class="table table-striped">
                <caption>List All Categories</caption>
                <thead>
                <tr>
                    <th>Name</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${categories}" var="category">
                    <tr>
                        <td>${category.name}</td>
                        <td><a href="<c:url value='/admin/category/edit/${category.id}' />">edit</a></td>
                        <td><a href="<c:url value='/admin/category/delete/${category.id}' />">delete</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <a href="<c:url value='/admin/category/add' />">Add Category</a>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>