package com.codeenginestudio.craftbeerapp.repositories;


import com.codeenginestudio.craftbeerapp.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByType(String type);
}
