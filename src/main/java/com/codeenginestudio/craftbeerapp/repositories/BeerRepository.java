package com.codeenginestudio.craftbeerapp.repositories;

import com.codeenginestudio.craftbeerapp.model.Beer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface BeerRepository extends JpaRepository<Beer, Integer> {
    List<Beer> findByState(String state);
}
