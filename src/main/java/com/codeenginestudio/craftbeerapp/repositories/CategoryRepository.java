package com.codeenginestudio.craftbeerapp.repositories;

import com.codeenginestudio.craftbeerapp.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

    Category findByName(String name);

    @Query("SELECT c FROM Category c JOIN FETCH c.beers WHERE c.id = (:id)")
    public Category findByIdAndFetchBeersEagerly(@Param("id") int id);
}
