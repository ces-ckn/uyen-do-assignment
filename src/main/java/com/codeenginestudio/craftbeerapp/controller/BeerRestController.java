package com.codeenginestudio.craftbeerapp.controller;

import com.codeenginestudio.craftbeerapp.enums.State;
import com.codeenginestudio.craftbeerapp.model.Beer;
import com.codeenginestudio.craftbeerapp.service.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class BeerRestController {

    @Autowired
    BeerService beerService;

    @RequestMapping(value = "/beer", method = RequestMethod.GET)
    public ResponseEntity<List<Beer>> listAllCategory(){
        List<Beer> beers = beerService.findByState(State.ACTIVE.getState());
        if (beers.isEmpty()){
            return new ResponseEntity<List<Beer>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Beer>>(beers, HttpStatus.OK);
    }

    @RequestMapping(value = "/beer/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Beer> getCategory(@PathVariable("id") int id){
        Beer category = beerService.findById(id);
        if (category == null){
            return new ResponseEntity<Beer>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Beer>(category, HttpStatus.OK);
    }

    @RequestMapping(value = "/beer", method = RequestMethod.POST)
    public ResponseEntity<Void> createCategory(@RequestBody Beer category, UriComponentsBuilder ucBuilder){
        beerService.save(category);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/category/{id}").buildAndExpand(category.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/beer/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Beer> updateCategory(@PathVariable("id") int id, @RequestBody Beer beer){
        Beer currentBeer = beerService.findById(id);

        if (currentBeer == null){
            return new ResponseEntity<Beer>(HttpStatus.NOT_FOUND);
        }

        currentBeer.setName(beer.getName());
        currentBeer.setCategory(beer.getCategory());
        currentBeer.setCountry(beer.getCountry());
        currentBeer.setManufacturer(beer.getManufacturer());
        currentBeer.setPrice(beer.getPrice());
        currentBeer.setState(beer.getState());
        beerService.save(currentBeer);
        return new ResponseEntity<Beer>(currentBeer, HttpStatus.OK);
    }

    @RequestMapping(value = "/beer/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Beer> deleteCategory(@PathVariable("id") int id){
        Beer category = beerService.findById(id);
        if (category == null){
            return new ResponseEntity<Beer>(HttpStatus.NOT_FOUND);
        }
        beerService.delete(id);
        return new ResponseEntity<Beer>(HttpStatus.NO_CONTENT);
    }

}
