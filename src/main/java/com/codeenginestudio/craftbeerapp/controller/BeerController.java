package com.codeenginestudio.craftbeerapp.controller;

import com.codeenginestudio.craftbeerapp.enums.State;
import com.codeenginestudio.craftbeerapp.model.Beer;
import com.codeenginestudio.craftbeerapp.model.Category;
import com.codeenginestudio.craftbeerapp.service.BeerService;
import com.codeenginestudio.craftbeerapp.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class BeerController {

    @Autowired
    BeerService beerService;

    @Autowired
    CategoryService categoryService;

    @RequestMapping(value = "/beer", method = RequestMethod.GET)
    public String listBeers(ModelMap model){
        List<Beer> beers = beerService.findAll();
        model.addAttribute("beers", beers);
        return "listBeers";
    }

    @RequestMapping(value = "/beer/add", method = RequestMethod.GET)
    public String addBeer(ModelMap model){
        Beer beer = new Beer();
        model.addAttribute("beer", beer);
        return "beer";
    }

    @RequestMapping(value = "/beer/add", method = RequestMethod.POST)
    public String saveBeer(@Valid Beer beer, BindingResult result, ModelMap model){
        String success = "";
        if (result.hasErrors()){
            return "beer";
        }

        if (beer.getId() != 0){
            Beer update = beerService.findById(beer.getId());
            update.setName(beer.getName());
            beerService.save(update);
            success = "Beer " + update.getName() + " has been updated successfully";
        }else {
            beerService.save(beer);
            success = "Beer " + beer.getName() + " has been created successfully";
        }
        List<Beer> beers = beerService.findAll();
        model.addAttribute("beers", beers);
        model.addAttribute("success", success);
        return "listBeers";
    }

    @RequestMapping(value = "/beer/edit/{id}", method = RequestMethod.GET)
    public String updateBeer(@PathVariable("id") int id, ModelMap model){
        Beer beer = beerService.findById(id);
        model.addAttribute("beer", beer);
        return "beer";
    }

    @RequestMapping(value = "/beer/archive/{id}", method = RequestMethod.GET)
    public String archiveBeer(@PathVariable("id") int id, ModelMap model){
        String success = "";
        Beer update = beerService.findById(id);
        if (update.getState().equals(State.ACTIVE.getState())){
            update.setState(State.ARCHIVE.getState());
        }else{
            update.setState(State.ACTIVE.getState());
        }
        success = "Beer " + update.getName() + " has been updated successfully";
        beerService.save(update);
        List<Beer> beers = beerService.findAll();
        model.addAttribute("beers", beers);
        model.addAttribute("success", success);
        return "listBeers";

    }

    @RequestMapping(value = "/beer/delete/{id}", method = RequestMethod.GET)
    public String deleteCategory(@PathVariable("id") int id, ModelMap model){
        String success = "";
        beerService.delete(id);
        success = "Beer has been deleted successfully";
        List<Beer> beers = beerService.findAll();
        model.addAttribute("beers", beers);
        model.addAttribute("success", success);
        return "listBeers";
    }

    @ModelAttribute("categories")
    public List<Category> initializeCategories(){
        return categoryService.findAll();
    }
}
