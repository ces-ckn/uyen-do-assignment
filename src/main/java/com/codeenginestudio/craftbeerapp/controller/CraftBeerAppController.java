package com.codeenginestudio.craftbeerapp.controller;

import com.codeenginestudio.craftbeerapp.model.Role;
import com.codeenginestudio.craftbeerapp.model.User;
import com.codeenginestudio.craftbeerapp.service.RoleService;
import com.codeenginestudio.craftbeerapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/")
public class CraftBeerAppController {

    @Autowired
    RoleService roleService;

    @Autowired
    UserService userService;

    @RequestMapping(value = { "/", "/home"} ,method = RequestMethod.GET)
    public String homePage(ModelMap model){
        model.addAttribute("greeting", "Hi, Welcome to mysite");
        return "welcome";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String adminPage(ModelMap model){
        model.addAttribute("user", getPrincipal());
        List<User> listUsers = userService.findAll();
        model.addAttribute("listUsers", listUsers);
        return "admin";
    }

    @RequestMapping(value = "/dba", method = RequestMethod.GET)
    public String dbaPage(ModelMap model){
        model.addAttribute("user", getPrincipal());
        return "dba";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(){

        return "login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }

    @RequestMapping(value = "/accessDenied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "accessDenied";
    }

    @RequestMapping(value = "/admin/user/add", method = RequestMethod.GET)
    public String newRegistration(ModelMap model){
        User user = new User();
        model.addAttribute("user", user);
        return "newuser";
    }

    @RequestMapping(value = "/admin/user/add", method = RequestMethod.POST)
    public String saveRegistration(@Valid User user, BindingResult result, ModelMap model){
        String success = "";
        if (result.hasErrors()){
            return "newuser";
        }
        if (user.getId() != 0 ){
            User update = userService.findById(user.getId());
            update.setState(user.getState());
            update.setEmail(user.getEmail());
            update.setFirstName(user.getFirstName());
            update.setLastName(user.getLastName());
            update.setRoles(user.getRoles());
            update.setUserName(user.getUserName());
            userService.save(update);
            success = "User " + user.getFirstName() + " has been updated successfully";
        }else{
            userService.save(user);
            success = "User " + user.getFirstName() + " has been registered successfully";
        }

        if(user.getRoles()!= null){
            for(Role role: user.getRoles()){
                System.out.println("Profile : "+ role.getType());
            }
        }
        List<User> listUsers = userService.findAll();
        model.addAttribute("listUsers", listUsers);
        model.addAttribute("success", success);
        return "admin";
    }

    @RequestMapping(value = "/admin/user/delete/{id}")
    public String deleteUser(@PathVariable("id") int id){
        userService.delete(id);
        return "redirect:/admin";
    }

    @RequestMapping(value = "/admin/user/edit/{id}")
    public String updateUser(@PathVariable("id") int id, ModelMap model){
        User user = userService.findById(id);
        model.addAttribute("user", user);
        return "newuser";
    }

    @RequestMapping(value = "/admin/user", method = RequestMethod.GET)
    public String listUser(ModelMap model){
        List<User> listUsers = userService.findAll();
        model.addAttribute("listUsers", listUsers);
        return "listuser";
    }

    private String getPrincipal(){
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails){
            userName = ((UserDetails) principal).getUsername();
        }else {
            userName = principal.toString();
        }
        return userName;
    }

    @ModelAttribute("roles")
    public List<Role> initializeProfiles(){
        return roleService.findAll();
    }
}
