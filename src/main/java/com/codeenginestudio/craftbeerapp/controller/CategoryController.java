package com.codeenginestudio.craftbeerapp.controller;


import com.codeenginestudio.craftbeerapp.model.Beer;
import com.codeenginestudio.craftbeerapp.model.Category;
import com.codeenginestudio.craftbeerapp.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.nio.channels.FileChannel;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @RequestMapping(value = "/category", method = RequestMethod.GET)
    public String listCategories(ModelMap model){
        List<Category> categories = categoryService.findAll();
        model.addAttribute("categories", categories);
        return "listCategories";
    }

    @RequestMapping(value = "/category/add", method = RequestMethod.GET)
    public String addCategory(ModelMap model){
        Category category = new Category();
        model.addAttribute("category", category);
        return "category";
    }

    @RequestMapping(value = "/category/add", method = RequestMethod.POST)
    public String saveCategory(@Valid Category category, BindingResult result, ModelMap model){
        if (result.hasErrors()){
            return "category";
        }

        if (category.getId() != 0){
            Category update = categoryService.findById(category.getId());
            update.setName(category.getName());
            categoryService.save(update);
        }else {
            categoryService.save(category);
        }
        return "redirect:/admin/category";
    }

    @RequestMapping(value = "/category/edit/{id}", method = RequestMethod.GET)
    public String updateCategory(@PathVariable("id") int id, ModelMap model){
        Category category = categoryService.findById(id);
        model.addAttribute("category", category);
        return "category";
    }

    @RequestMapping(value = "/category/delete/{id}", method = RequestMethod.GET)
    public String deleteCategory(@PathVariable("id") int id, ModelMap model){
        String warning = "";
        String success = "";
        Category category = categoryService.findByIdAndFetchBeersEagerly(id);
        if (category == null){
            categoryService.delete(id);
            success = "Category has been deleted successfully";
            model.addAttribute("success", success);
        }else {
            warning = "Can not delete Category not empty";
            model.addAttribute("warning", warning);
        }

        List<Category> categories = categoryService.findAll();
        model.addAttribute("categories", categories);
        return "listCategories";
    }
}
