package com.codeenginestudio.craftbeerapp.service;


import com.codeenginestudio.craftbeerapp.model.Beer;

import java.util.List;

public interface BeerService {

    List<Beer> findAll();

    void save(Beer beer);

    Beer findById(int id);

    void delete(int id);

    List<Beer> findByState(String state);
}
