package com.codeenginestudio.craftbeerapp.service;

import com.codeenginestudio.craftbeerapp.model.User;

import java.util.List;

/**
 * Created by dominhuyen on 9/21/15.
 */
public interface UserService {

    User findById(int id);

    User findByUserName(String username);

    void save(User user);

    void delete(int id);

    List<User> findAll();
}
