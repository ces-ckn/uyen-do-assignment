package com.codeenginestudio.craftbeerapp.service;

import com.codeenginestudio.craftbeerapp.model.Beer;
import com.codeenginestudio.craftbeerapp.repositories.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("beerService")
@Transactional
public class BeerServiceImpl implements BeerService {

    @Autowired
    BeerRepository repository;

    @Override
    public List<Beer> findAll() {
        return repository.findAll();
    }

    @Override
    public void save(Beer beer) {
        repository.save(beer);
    }

    @Override
    public Beer findById(int id) {
        return repository.findOne(id);
    }

    @Override
    public void delete(int id) {
        repository.delete(id);
    }

    @Override
    public List<Beer> findByState(String state) {
        return repository.findByState(state);
    }
}
