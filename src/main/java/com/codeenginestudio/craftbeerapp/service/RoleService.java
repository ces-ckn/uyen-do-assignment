package com.codeenginestudio.craftbeerapp.service;


import com.codeenginestudio.craftbeerapp.model.Role;

import java.util.List;

public interface RoleService {

    List<Role> findAll();

    Role findByType(String type);

    Role findById(int id);
}
