package com.codeenginestudio.craftbeerapp.service;

import com.codeenginestudio.craftbeerapp.model.Role;
import com.codeenginestudio.craftbeerapp.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("roleService")
@Transactional
public class RoleServiceImpl implements RoleService{

    @Autowired
    RoleRepository repository;

    @Override
    public List<Role> findAll() {
        return (List<Role>) repository.findAll();
    }

    @Override
    public Role findByType(String type) {
        return repository.findByType(type);
    }

    @Override
    public Role findById(int id) {
        return repository.findOne(id);
    }
}
