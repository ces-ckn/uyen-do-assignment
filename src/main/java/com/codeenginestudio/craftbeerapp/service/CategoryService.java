package com.codeenginestudio.craftbeerapp.service;


import com.codeenginestudio.craftbeerapp.model.Category;

import java.util.List;

public interface CategoryService {

    List<Category> findAll();

    Category findById(int id);

    Category findByIdAndFetchBeersEagerly(int id);

    Category findByName(String name);

    void delete(int id);

    void save(Category category);
}
