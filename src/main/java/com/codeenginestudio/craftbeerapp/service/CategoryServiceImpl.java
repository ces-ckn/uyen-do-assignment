package com.codeenginestudio.craftbeerapp.service;

import com.codeenginestudio.craftbeerapp.model.Category;
import com.codeenginestudio.craftbeerapp.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("categoryService")
@Transactional
public class CategoryServiceImpl implements CategoryService{

    @Autowired
    CategoryRepository repository;

    @Override
    public List<Category> findAll() {
        return repository.findAll();
    }

    @Override
    public Category findById(int id) {
        return repository.findOne(id);
    }

    @Override
    public Category findByIdAndFetchBeersEagerly(int id) {
        return repository.findByIdAndFetchBeersEagerly(id);
    }

    @Override
    public Category findByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public void delete(int id) {
        repository.delete(id);
    }

    @Override
    public void save(Category category) {
        repository.save(category);
    }
}
