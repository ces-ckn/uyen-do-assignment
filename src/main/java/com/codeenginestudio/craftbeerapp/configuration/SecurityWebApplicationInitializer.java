package com.codeenginestudio.craftbeerapp.configuration;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by dominhuyen on 9/18/15.
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

}
