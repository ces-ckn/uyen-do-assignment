package com.codeenginestudio.craftbeerapp.configuration;

import com.codeenginestudio.craftbeerapp.model.Role;
import com.codeenginestudio.craftbeerapp.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by dominhuyen on 9/20/15.
 */

@Component
public class RoleToUserRoleConverter implements Converter<Object, Role> {

    @Autowired
    RoleService roleService;

    /*
     * Gets UserProfile by Id
     * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
     */
    @Override
    public Role convert(Object o) {
        Integer id = Integer.parseInt((String)o);
        Role role = roleService.findById(id);
        System.out.println("Role : "+role);
        return role;
    }
}
