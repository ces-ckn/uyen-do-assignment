package com.codeenginestudio.craftbeerapp.enums;

/**
 * Created by dominhuyen on 9/21/15.
 */
public enum RoleType {

    USER("USER"),
    DBA("DBA"),
    ADMIN("ADMIN");

    String roleType;

    private RoleType(String roleType){

        this.roleType = roleType;
    }

    public String getUserProfileType(){

        return roleType;
    }
}
