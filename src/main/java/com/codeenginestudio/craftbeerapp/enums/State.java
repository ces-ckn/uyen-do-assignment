package com.codeenginestudio.craftbeerapp.enums;

/**
 * Created by dominhuyen on 9/21/15.
 */
public enum State {

    ACTIVE("ACTIVE"),
    INACTIVE("INACTIVE"),
    DELETED("DELETED"),
    ARCHIVE("ARCHIVE"),
    LOCKED("LOCKED");

    private String state;

    private State(final String state){

        this.state = state;
    }

    public String getState(){

        return this.state;
    }

    @Override
    public String toString(){

        return this.state;
    }

    public String getName(){

        return this.name();
    }
}
