INSERT INTO APP_USER_ROLE (user_id, role_id)
SELECT u.id, role.id FROM app_user u, app_role role
where u.username='bill' and role.type='USER';

INSERT INTO APP_USER_ROLE (user_id, role_id)
SELECT u.id, role.id FROM app_user u, app_role role
where u.username='danny' and role.type='USER';

INSERT INTO APP_USER_ROLE (user_id, role_id)
SELECT u.id, role.id FROM app_user u, app_role role
where u.username='sam' and role.type='ADMIN';

INSERT INTO APP_USER_ROLE (user_id, role_id)
SELECT u.id, role.id FROM app_user u, app_role role
where u.username='nicole' and role.type='DBA';

INSERT INTO APP_USER_ROLE (user_id, role_id)
SELECT u.id, role.id FROM app_user u, app_role role
where u.username='kenny' and role.type='ADMIN';

INSERT INTO APP_USER_ROLE (user_id, role_id)
SELECT u.id, role.id FROM app_user u, app_role role
where u.username='kenny' and role.type='DBA';